<?php

namespace Test;

use App\Example;
use PHPUnit\Framework\TestCase;

class ExampleTest extends TestCase
{
    public function testItCanNotBeConstructed() {
        $this->expectException(\RuntimeException::class);
        new Example();
    }
}
