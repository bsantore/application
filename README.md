# README

Welcome!

We are going to use this repository to build our application.

You'll find directions inside DIRECTIONS.md

- The `src` directory will hold your work, with a namespace of `App`.
- The `tests` directory will hold your tests, with a namespace of `Test`.


## Build Environments

OPTIONAL:
If you'd like to use the provided image, you may use the following

`docker load < app.img`

This will provide a Docker image to work with, and run your application.