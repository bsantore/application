<?php

namespace App;

use RuntimeException;

class Example
{
    public function __construct()
    {
        throw new RuntimeException();
    }
}